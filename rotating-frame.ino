#include <WiFi.h>
#include <PubSubClient.h>
#include <SPI.h>
#include <ArduinoJson.h>

//Include Settings
#include "settings.h"

//Pins
int blue = 27; 
int pink = 14; 
int orange = 12; 
int yellow = 13;
int pinnumber = 4;
//Motor settings
int steps = 0;
int full_rotation = 0;
int targetstep = 2048;

//step settings
int pins[] = {blue , pink, orange, yellow};
int full_step[][4] = {
	{HIGH, HIGH, LOW, LOW},
	{LOW, HIGH, HIGH, LOW},
	{LOW, LOW, HIGH, HIGH},
	{HIGH, LOW, LOW, HIGH}
};
int half_step[][4] = {
	{HIGH, LOW, LOW, LOW},
	{HIGH, HIGH, LOW, LOW},
	{LOW, HIGH, LOW, LOW},
	{LOW, HIGH, HIGH, LOW},
	{LOW, LOW, HIGH, LOW},
	{LOW, LOW, HIGH, HIGH},
	{LOW, LOW, LOW, HIGH},
	{HIGH, LOW, LOW, HIGH},
};

int count = 0;

//Set up PupSubClient
WiFiClient espClient;
PubSubClient client(espClient);


//Selber geschriebene Funktionen
void step(int step[][4],int angle = 360, bool clockwise = true, int stepnumber = 4, int speed = 0) {
	// int states = sizeof(step) / sizeof(step[0]);
	int turns = 0;
	Serial.print("rotate ");
	Serial.print(angle);
	Serial.print("°");
	Serial.println();
	int target = angle * 5.688888889;


	while (turns < target) {
		int sequence = count % stepnumber;
		if(!clockwise) {
			sequence = (stepnumber - 1) - sequence;
		}
		for(int pin = 0; pin < pinnumber; pin++) {
			digitalWrite(pins[pin], step[pin][sequence]);
		}
		count++;
		turns++;
		delay(2);
		delay(speed);
	}
	for(int pin = 0; pin < pinnumber; pin++) {
		digitalWrite(pins[pin], LOW);
	}
	Serial.println("target reached");

}




//Set up callback for PubSub

void callback(char* topic, byte* payload, unsigned int length) {
	Serial.print("Message arrived [");
	Serial.print(topic);
	Serial.print("] ");
	payload[length] = '\0';
	String newPayload = String((char *)payload);
	int intPayload = newPayload.toInt();
	Serial.println(newPayload);
	Serial.println();
	
	StaticJsonDocument<256> doc;
  	DeserializationError error = deserializeJson(doc, payload, length);

	// Test if parsing succeeds.
	if (error) {
		Serial.print(F("deserializeJson() failed: "));
		Serial.println(error.c_str());
		return;
	}
	int degree = doc["degree"];
	bool clockwise = doc["clockwise"];
	int speed = doc["speed"];
	const char* steps = doc["steps"];
	int stepnumber = sizeof(full_step) / sizeof(full_step[0]);
	Serial.print("Degree; ");
	Serial.print(degree);
	Serial.println();
	Serial.print("clockwise: ");
	Serial.print(clockwise);
	Serial.println();

	step(full_step, degree, clockwise, stepnumber, speed);


}



//
//Mqtt connect
void mqtt_connect(){
	while (!client.connected()) {
		if (client.connect (device, user, mqtt_password)){
			Serial.println("Mqtt connected: Listen to");
			Serial.print(MQTT_TOPIC);
			client.subscribe(MQTT_TOPIC);
		}
		else {
			Serial.println("Connection failed");
			Serial.println(client.state());
			Serial.println("wait");
			delay(5000);
		}
	}
}



void setup() {
	//general Stuff
	Serial.begin(115200);
	for(int pin = 0; pin < pinnumber; pin++ ) {
		pinMode(pins[pin], OUTPUT);
		digitalWrite(pins[pin], LOW);
	}


	//WiFi Set up
	WiFi.config(ip, gateway, subnet, DNS);
	WiFi.mode(WIFI_STA);
	WiFi.begin(ssid, password);
	while (WiFi.status() != WL_CONNECTED){
		delay(700);
		Serial.println("Conecting to WiFi");
	}
	Serial.println("Connected to WiFi");
	Serial.println(WiFi.localIP());

	//Mqtt Set up
	client.setCallback(callback);
	client.setServer(mqtt_server, 1883);
	mqtt_connect();

	// int size_of = sizeof(full_step);
	// int size_entry = sizeof(full_step[0]);
	// Serial.println(size_of);
	// Serial.println(size_entry);
	// Serial.println("Devide");
	// Serial.println(size_of / size_entry);
}


void loop() {
	if (!client.connected()){
		mqtt_connect();
	}
	client.loop();
}
